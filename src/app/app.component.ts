import { Component, OnInit } from '@angular/core';
import { AppService } from './app.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Previsão do Tempo';

  card;

  city;

  cities = [];

  constructor(public appService: AppService) {

  }

  ngOnInit() {
  	this.getCapitalList();
  }

  getCity(city) {
  	var scope = this;
  	this.appService.getCity(city)
  	.toPromise()
  	.then(function (data: any) {
  		const result = JSON.parse(data._body);
  		scope.city = result[0];
  		scope.getCityForecast(result[0].Key);
  	});
  }

  getCityForecast(key) {
  	const scope = this;
  	this.appService.getCityForecast(key, true)
  	.toPromise()
  	.then(function(forecast: any) {
  		const result = JSON.parse(forecast._body);
  		scope.card = result;
  		console.log('forecast', scope.card)
  	});
  }

  getDailyCityForecast(key, cityName) {
  	const scope = this;
  	this.appService.getDailyCityForecast(key, false)
  	.toPromise()
  	.then(function(forecast: any) {
  		const result = JSON.parse(forecast._body);
  		const capital = {
  			city: cityName,
  			min: parseInt(result.DailyForecasts[0].Temperature.Minimum.Value),
  			max: parseInt(result.DailyForecasts[0].Temperature.Maximum.Value)
  		}
  		scope.cities.push(capital);
  	});
  }

  parseTemperature(temperature) {
  	return parseInt(temperature);
  }

  getDay(forecastDate) {
  	const weekDay = ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'];
  	const date = new Date(forecastDate);
  	return weekDay[date.getDay()];
  }

  getCapitalList() {
  	const scope = this;
  	const capitalList = this.appService.getCapitalList();

  	for (let capital of capitalList) {
  		this.appService.getCity(capital)
	  	.toPromise()
	  	.then(function(data: any) {
	  		const result = JSON.parse(data._body);
	  		scope.getDailyCityForecast(result[0].Key, result[0].LocalizedName);
	  	});
  	}
  	
  }
}
