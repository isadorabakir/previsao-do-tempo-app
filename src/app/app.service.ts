import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  constructor(private http: Http) { }

  getCapitalList() {
  	  const capitalList = [
	  	'Rio de Janeiro',
	  	'São Paulo',
	  	'Belo Horizonte',
	  	'Brasília',
	  	'Belém',
	  	'Salvador',
	  	'Curitiba',
	  	'Fortaleza',
	  	'Manaus',
	  	'João Pessoa'
	  ];
	  return capitalList;
  }

  apiKey = 'gdA0d2rDHxeUDJjCcUjL0RzY6g62oc6s';

  getCityForecast(city, details) {
	return this.http.get('http://dataservice.accuweather.com/forecasts/v1/daily/5day/'+city+'?apikey='+this.apiKey+'&metric=true&details='+details+'&language=pt');
  }

  getDailyCityForecast(city, details) {
	return this.http.get('http://dataservice.accuweather.com/forecasts/v1/daily/1day/'+city+'?apikey='+this.apiKey+'&metric=true&details='+details+'&language=pt');
  }

  getCity(city) {
  	return this.http.get('http://dataservice.accuweather.com/locations/v1/cities/search?apikey='+this.apiKey+'&q='+city);
  }
}






